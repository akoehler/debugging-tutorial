(function() {

  var employeesData;
  var sum = 0;

  // Get employees data
  $.getJSON('employees.json').done(function (data) {
    employeesData = data.employees;
  });

  function add(numbers) {

    if (numbers) {
      numbers = numbers.split(',');
    }

    for (var i = 0; i < numbers.length; i++) {
      sum += numbers[i];
    }

    return sum;
  }

  /**
   * Validates the correct result displays when using the Add function
   */
  function validate() {

    function add(a,b) {
      return a + b;
    }

    var error = false;
    var sum = $('.debugger-3-input').val().split(',').map( function (val) {
      return parseInt(val);
    }).reduce(add, 0);

    if ($('.debugger-3-result').html() === '' || parseInt($('.debugger-3-result').html()) !== sum) {
      error = true;
    }

    if (error) {
      $('.error').show();
    } else {
      $('.error').hide();
    }
  }

  // jQuery bindings
  $(function() {

    $('.alert-1').click(function () {
      alert('Hello!');
    });

    $('.alert-2').click(function () {
      alert(employeesData);
    });

    $('.console-1').click(function () {
      console.log('Hello');
    });

    $('.console-2').click(function () {
      console.log(employeesData);
    });

    $('.console-3').click(function () {
      console.table(employeesData);
    });

    $('.console-4').click(function () {
      console.table(employeesData, 'FirstName');
    });

    $('.console-5').click(function () {
      console.error('This is an error');
    });

    $('.console-6').click(function () {
      console.info('This is an informational message');
    });

    $('button.debugger-1').click(function (e) {
      debugger;
    });

    $('button.debugger-2').click(function (e) {
      // Put breakpoint here and step over each line
      $(this).css({'background-color': 'blue'});
      $(this).css({'background-color': 'red'});
      $(this).css({'background-color': 'yellow'});
      $(this).css({'background-color': 'black'});
      $(this).css({'background-color': ''});
    });

    $('button.debugger-3').click(function (e) {
      var sum = add($('.debugger-3-input').val());

      $('debugger-3-result').html(sum);

      validate();
    });
  });
})();